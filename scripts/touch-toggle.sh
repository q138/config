#!/bin/bash

TOGGLE=.toggle

if [ ! -e $TOGGLE ]; then
   touch $TOGGLE
  xinput set-prop 'FocalTechPS/2 FocalTech Touchpad' 'Device Enabled' 0
else
  rm $TOGGLE
  xinput set-prop 'FocalTechPS/2 FocalTech Touchpad' 'Device Enabled' 1
fi
