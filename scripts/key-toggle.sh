#!/bin/bash

TOGGLED=.toggled

if [ ! -e $TOGGLED ]; then
    touch $TOGGLED
    xinput set-prop 'AT Translated Set 2 keyboard' 'Device Enabled' 1
else
    rm $TOGGLED
    xinput set-prop 'AT Translated Set 2 keyboard' 'Device Enabled' 0
fi
